# Dzhanibekov-Effect-with-EGM96-Earth-Model-Blender-Simulation-
The Dzhanibekov Effect using an exagerated EGM96 representation of the earth in a blender file.

To use the .blend file, 
1. open it in blender, 
2. press 'p' to start the animation, or select Game->Start Game Engine from the top tool bar,
3. press 's' to speed up the orbit, 
4. press 'x' to slow it down again


## Dzhanibekov Effect applied to Earth Gravitational Model (EGM96)

[![Dzhanibekov Effect applied to Earth Gravitational Model (EGM96)](https://img.youtube.com/vi/J-l-SbCFhL0/0.jpg)](https://youtu.be/J-l-SbCFhL0)


## Credit

If you use this model please credit with a link to this page.

https://gitlab.com/seanwasere/Dzhanibekov-Effect-with-EGM96-Earth-Model-Blender-Simulation-

Please like, comment, subscribe and share, never forget to share.

